//
//  AppDelegate.swift
//  Eiya
//
//  Created by Dinesh Kumar on 8/28/15.
//  Copyright (c) 2015 Dinesh Kumar. All rights reserved.
//

import UIKit
import FBSDKCoreKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        self.window?.backgroundColor = UIColor.whiteColor()
        
        if(NSUserDefaults .standardUserDefaults().valueForKey(loggedInUserFacebookId) != nil){
            openImageSelectOptionOrWearViewForLoggedInUser()
        }
        
            // Specify the notification types.
            let notificationTypes: UIUserNotificationType = [UIUserNotificationType.Alert, UIUserNotificationType.Sound]
        let shoppingListReminderCategory = UIMutableUserNotificationCategory()
        shoppingListReminderCategory.identifier = "currentDayWhatToWearSuggestion"
        
        
        let categoriesForSettings = NSSet(objects: shoppingListReminderCategory)
        let newNotificationSettings = UIUserNotificationSettings(forTypes: notificationTypes, categories: categoriesForSettings as? Set<UIUserNotificationCategory>)
        UIApplication.sharedApplication().registerUserNotificationSettings(newNotificationSettings)
        //        if (application.applicationState ==  UIApplicationState.Inactive || application.applicationState == UIAppli
        
        // Override point for customization after application launch.
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }

    
    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
        
        print(notificationSettings.types.rawValue)
    }
    
    
    func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {
        // Do something serious in a real app.
        print("Received Local Notification:")
        if (application.applicationState ==  UIApplicationState.Inactive || application.applicationState == UIApplicationState.Background) {
            
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = storyboard.instantiateViewControllerWithIdentifier("wearStyleView") as! WearSuggestTionViewController
        let rootNavigation = UINavigationController(rootViewController: initialViewController)
        initialViewController.isThisViewHomeScreen = false
        self.window?.rootViewController = rootNavigation
        self.window?.makeKeyAndVisible()
        }
    }
    
    
    func application(application: UIApplication, handleActionWithIdentifier identifier: String?, forLocalNotification notification: UILocalNotification, completionHandler: () -> Void) {
        
        completionHandler()
    }

    
    //MARK: - OPEN HOME SCREEN
    
    func openImageSelectOptionOrWearViewForLoggedInUser(){
        
        if(currentUserTotalTShirts().count > 0 && currentUserTotalPants().count > 0){
            
           openWearSuggestionView()
        }else{
            openImageOptionView()
        }
    }
    
    //MARK: - OPEN WEAR SUGGESTION VIEW
    
    func openWearSuggestionView(){
    
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let initialViewController = storyboard.instantiateViewControllerWithIdentifier("wearStyleView") as! WearSuggestTionViewController
    let rootNavigation = UINavigationController(rootViewController: initialViewController)
    initialViewController.isThisViewHomeScreen = true
    self.window?.rootViewController = rootNavigation
    self.window?.makeKeyAndVisible()
    }
    
    //MARK: - OPEN IMAGE CHOOSE SCREEN IF USER HAS NOT ADDED ANY COLLECTION OR HE/ SHE WANT TO ADD MORE
    
    func  openImageOptionView(){
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = storyboard.instantiateViewControllerWithIdentifier("imageChooseView") as! ImageChooseViewController
        
        let rootNavigation = UINavigationController(rootViewController: initialViewController)
        self.window?.rootViewController = rootNavigation
        self.window?.makeKeyAndVisible()
    }
    
    
    //MARK: - COMMON LOGOUT FUNCTION
    func logoutFunction(){
        
        UIApplication.sharedApplication().cancelAllLocalNotifications()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = storyboard.instantiateViewControllerWithIdentifier("splashView") as! ViewController
        let rootNavigation = UINavigationController(rootViewController: initialViewController)
        self.window?.rootViewController = rootNavigation
        self.window?.makeKeyAndVisible()
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool
    {
        return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
    }
}

