//
//  Keys.swift
//  Eiya-CP
//

import Foundation
import UIKit
//import GoogleMaps


func println(object: Any) {
	Swift.print(object)
}
func print(object: Any) {
    Swift.print(object)
}


//Error Message
let ErrorInternetConnection                     = "You may not have internet connection. Please check your connection and try again."
let ConnectionError                             = "Could not connect to the server. Please try again later."
let loggedInUserFacebookId                       = "savingUserIdForParsing"

//Version
var OSVersion                                   = (UIDevice.currentDevice().systemVersion as NSString).doubleValue


//MARK: Storyboard
let MainStoryboard                              = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle())

