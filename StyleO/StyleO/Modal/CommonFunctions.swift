//
//  CommonFunctions.swift

import Foundation
import UIKit
//MARK: Alert Display Function

struct MoveKeyboard {
    static let KEYBOARD_ANIMATION_DURATION : CGFloat = 0.3
    static let MINIMUM_SCROLL_FRACTION : CGFloat = 0.2;
    static let MAXIMUM_SCROLL_FRACTION : CGFloat = 0.8;
    static let PORTRAIT_KEYBOARD_HEIGHT : CGFloat = 216;
    static let LANDSCAPE_KEYBOARD_HEIGHT : CGFloat = 162;
}

var totalCasesArray: [Dictionary<String, AnyObject>] = []

//MARK: - COMMON ALERT FUNCTION
func Alert(ViewController : UIViewController , alertMessage : String) {
    
    let alertController = UIAlertController(title: "", message: alertMessage, preferredStyle: .Alert)
    let cancelAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel) { (action) in
        return
    }
    alertController.addAction(cancelAction)
    ViewController.presentViewController(alertController, animated: true) {
    }
}

//MARK: - DELETE FILE AT THIS PATH
func deleteFileAtPath(yourPath:String){
    
    do {
        try  NSFileManager.defaultManager().removeItemAtPath(yourPath)
    } catch let error as NSError {
        print(error.localizedDescription)
    }
}

//MARK: - CURRENT USER TOTAL SHIRT RETURN FUNCTION
func currentUserTotalTShirts() -> [String?] {
    var imageArray : [String?]
    
    imageArray = []
    var documentsUrl =  NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!
    documentsUrl = documentsUrl.URLByAppendingPathComponent("\(NSUserDefaults.standardUserDefaults().valueForKey(loggedInUserFacebookId) as! String)")
    documentsUrl = documentsUrl.URLByAppendingPathComponent("TShirts")

    do {
        let directoryUrls = try  NSFileManager.defaultManager().contentsOfDirectoryAtURL(documentsUrl, includingPropertiesForKeys: nil, options: NSDirectoryEnumerationOptions())
        let jpegFiles = directoryUrls.filter(){ $0.pathExtension == "jpeg" }.map{ $0.lastPathComponent }
        
        return jpegFiles
    } catch let error as NSError {
        print(error.localizedDescription)
    }
    return imageArray
}

//MARK: - CURRENT USER TOTAL PANT RETURN FUNCTION
func currentUserTotalPants() -> [String?] {
    var imageArray : [String?]
    imageArray = []
    var documentsUrl =  NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!
    
    documentsUrl = documentsUrl.URLByAppendingPathComponent("\(NSUserDefaults.standardUserDefaults().valueForKey(loggedInUserFacebookId) as! String)")
    
    documentsUrl = documentsUrl.URLByAppendingPathComponent("Pants")
    
    do {
        let directoryUrls = try  NSFileManager.defaultManager().contentsOfDirectoryAtURL(documentsUrl, includingPropertiesForKeys: nil, options: NSDirectoryEnumerationOptions())
        let jpegFiles = directoryUrls.filter(){ $0.pathExtension == "jpeg" }.map{ $0.lastPathComponent }
        return jpegFiles
        
    } catch let error as NSError {
        print(error.localizedDescription)
    }
    
    return imageArray
}


//MARK: - CURRETN USER PANTS FOLDER PATH RETURN FUNCTION
func currentUserPantFolder() -> String {
    
    let path = getDocumentsURL().stringByAppendingString("/\(NSUserDefaults.standardUserDefaults().valueForKey(loggedInUserFacebookId) as! String)/Pants")
    return path
}

//MARK: - CURRETN USER PANTS FOLDER PATH RETURN FUNCTION
func currentUserTShirtFolder() -> String {
    let path = getDocumentsURL().stringByAppendingString("/\(NSUserDefaults.standardUserDefaults().valueForKey(loggedInUserFacebookId) as! String)/TShirts")
    
    return path
}

//MARK: - CREATING COMBINATION FOR SELETED T SHIRTS & PANTS
func creatingCombinationSelected(shirts:[String?],pants:[String?])
{
    totalCasesArray.removeAll()
    for i in 0..<shirts.count {
        for j in 0..<pants.count {
            var newEntry =  Dictionary<String,AnyObject>()
            newEntry["tShirturl"] = shirts[i]
            newEntry[ "pantUrl"] = pants[j]
            totalCasesArray.append(newEntry)
        }
    }
}

//MARK: - GET DOCUMENT URL
func getDocumentsURL() -> AnyObject {
    
    let paths = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
    let documentsDirectory: AnyObject = paths[0]
    return documentsDirectory
}

//MARK: - COMMON FUNCTION TO CREATE FOLDER
func createFolderAtThisPath(yourPath:String,folderName:String){
    let dataPath = yourPath.stringByAppendingString("/\(folderName)")
    do {
        try NSFileManager.defaultManager().createDirectoryAtPath(dataPath, withIntermediateDirectories: false, attributes: nil)
    } catch let error as NSError {
        print(error.localizedDescription);
    }
}



