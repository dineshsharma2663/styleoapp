//
//  WearSuggestTionViewController.swift
//  StyleO
//
//  Created by Dinesh Kumar on 07/12/15.
//  Copyright © 2015 Dinesh Kumar. All rights reserved.
//

import UIKit

class WearSuggestTionViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {
    
    //MARK: - IBOUTLETS
    @IBOutlet weak var wearViewCollectionView: UICollectionView!
    @IBOutlet weak var addMoreLabel : UILabel!
    
    //MARK: - INITIALISE VARIABLES
    var totalCollection : [Dictionary<String, AnyObject>] = []
    var isThisViewHomeScreen:Bool!
    var currentOpenIndex : Int!
    
    //MARK: - VIEW APPEAR LOAD FUNCTIONS
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.sharedApplication().cancelAllLocalNotifications()
        
        scheduleLocalNotification()
        creatingCombinationSelected(currentUserTotalTShirts(), pants: currentUserTotalPants())
        totalCollection = totalCasesArray
        wearViewCollectionView.registerNib(UINib(nibName: "wearThisStyleView", bundle: NSBundle.mainBundle()), forCellWithReuseIdentifier: "wearCollectionView")
        
        if(totalCollection.count == 0){
            addMoreLabel.hidden = false
        }
        else{
            addMoreLabel.hidden = true
        }
    }
    //MARK: - SCHEDULE DAILY NOTIFICATION
    func scheduleLocalNotification() {
        let localNotification = UILocalNotification()
        localNotification.fireDate = NSDate()
        localNotification.alertBody = "Heyy...! Its time to pick your style for taday..."
        localNotification.repeatInterval = NSCalendarUnit.Day
        localNotification.category = "currentDayWhatToWearSuggestion"
        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
    }

    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBar.hidden=false
        self.navigationItem.title = "StyleO"
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "+ Add", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("addMoreAction:"))

        if (self.isThisViewHomeScreen == false){
            
            let backImage: UIImage = UIImage(named: "refresh")!
            let refreshButton = UIButton(type: UIButtonType.System)
            refreshButton.setBackgroundImage(backImage, forState: .Normal)
            refreshButton.frame = CGRectMake(0.0, 0.0, backImage.size.width,   backImage.size.height);
            refreshButton.addTarget(self, action: "notificationPressed:", forControlEvents: UIControlEvents.TouchUpInside)
            let rightbarButton =  UIBarButtonItem(customView: refreshButton)
            self.navigationItem.rightBarButtonItem = rightbarButton
        }
    }
    
    
    //MARK: - COLLECTION VIEW DELEGATES & DATA SOURCE
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return totalCollection.count
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width:UIScreen.mainScreen().bounds.width, height: UIScreen.mainScreen().bounds.height-70);
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
            return 0
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
            return 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let todayWearCell = collectionView.dequeueReusableCellWithReuseIdentifier("wearCollectionView", forIndexPath: indexPath) as! WearableTableCell
        
        let wearCombination = totalCollection[indexPath.row]
        var newEntry =  Dictionary<String,AnyObject>()
        newEntry = wearCombination
        
        //ASSIGN T SHIRT IMAGE FROM DOCUMENT PATH
        let tShirtUrl:String = newEntry["tShirturl"] as! String
        let tShirtImage = UIImage(contentsOfFile: currentUserTShirtFolder() + "/" + tShirtUrl)
        todayWearCell.tshirtImageView.image = tShirtImage
        
        //ASSIGN PANT IMAGE FROM DOCUMENT PATH
        let pantUrl:String = newEntry["pantUrl"] as! String
        let pantImage = UIImage(contentsOfFile: currentUserPantFolder() + "/" + pantUrl)
        todayWearCell.pantImageView.image = pantImage
        
        //SETTING DISLIKE BUTTON TITLE WITH INDEXPATH FOR FURTHER ACTIONS
        todayWearCell.dislikeButton.titleLabel?.numberOfLines = 1;
        currentOpenIndex = indexPath.row
        todayWearCell.dislikeButton.setTitle("\n\(indexPath.row)", forState: UIControlState.Normal)
        todayWearCell.dislikeButton.addTarget(self, action: "dislikeButtonPressed:", forControlEvents: UIControlEvents.TouchUpInside)
        
        todayWearCell.backgroundColor = UIColor.clearColor()
        return todayWearCell
    }
    
    //MARK: - DISLIKE BUTTON PRESSED
    func dislikeButtonPressed(sender:UIButton){
        
        let tempArr = (sender.currentTitle!).componentsSeparatedByString("\n") as NSArray
        let currentIndex = Int(tempArr.objectAtIndex(1) as! String)
        openNextStyleCombination(currentIndex!)
    }
    
    //MARK: - DELETE INDEX & UPDATE VIEW
    
    func openNextStyleCombination(currentIndex: Int){
        
        totalCollection.removeAtIndex(currentIndex)
        wearViewCollectionView.reloadData()
        scrollToParticularIndex()
    }
    
    //MARK: - SCROLL TO RANDOM INDEX AFTER DISLIKE
    func scrollToParticularIndex(){
        if(totalCollection.count>1){
            let randomIndex = Int(arc4random_uniform(UInt32(totalCollection.count)-1))
            wearViewCollectionView.scrollToItemAtIndexPath(NSIndexPath(forRow: randomIndex, inSection: 0), atScrollPosition: UICollectionViewScrollPosition.CenteredHorizontally, animated: false)
        }else{
            addMoreLabel.hidden = false
            wearViewCollectionView.reloadData()
        }
        
    }
    
    //MARK: - NOTIFICATION BUTTON PRESSED
    func notificationPressed(sender:UIButton){
        
        if (totalCollection.count>0){
            
            openNextStyleCombination(currentOpenIndex)

               }
    }
    
    //MARK: - ADD MORE BUTTON ACTION
    func addMoreAction(sender: UIButton) {
        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appdelegate.openImageOptionView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
