//
//  ViewController.swift
//  StyleO
//
//  Created by Dinesh Kumar on 06/12/15.
//  Copyright © 2015 Dinesh Kumar. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBar.hidden=true
    }
    
    //MARK: - IMAGE CHOOSE OPTION OPEN
    func imageChooseOpen(){
        
        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appdelegate.openImageSelectOptionOrWearViewForLoggedInUser()
        
    }
    
    //MARK: - CREATING FIRST USER FOLDER TO SAVE DATA
    func createOrFetchUserInfoDatabaseInDirectory(UserFaceBookId:String){
        
        let dataPath = getDocumentsURL().stringByAppendingString("/\(UserFaceBookId)")
        
        if (!NSFileManager.defaultManager().fileExistsAtPath(dataPath)) {
            do {
                try NSFileManager.defaultManager().createDirectoryAtPath(dataPath, withIntermediateDirectories: false, attributes: nil)
                createFolderAtThisPath(dataPath, folderName: "/TShirts")
                createFolderAtThisPath(dataPath, folderName: "/Pants")
                
            } catch let error as NSError {
                print(error.localizedDescription);
            }
        }
    }
    
    //MARK: - LOGIN WITH FACEBOOK BUTTON PRESSED
    @IBAction func loginWithFacebookButtonPressed(sender: UIButton) {
        if !IJReachability.isConnectedToNetwork() {
            Alert(self, alertMessage: ErrorInternetConnection)
        }
        else {
            let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
            fbLoginManager.logInWithReadPermissions(["email"], fromViewController: self, handler: { (result, error) -> Void in
                if (error == nil){
                    let fbloginresult : FBSDKLoginManagerLoginResult = result
                    if(fbloginresult.grantedPermissions != nil)
                    {
                        if(fbloginresult.grantedPermissions.contains("email"))
                        {
                            if((FBSDKAccessToken.currentAccessToken()) != nil){
                                FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).startWithCompletionHandler({ (connection, result, error) -> Void in
                                    if (error == nil){
                                        println(result)
                                        if let extractedResult = result as? NSDictionary {
                                            if let extractedID = extractedResult["id"]! as? NSString {
                                                NSUserDefaults.standardUserDefaults().setValue(extractedID, forKey: loggedInUserFacebookId)
                                                self.createOrFetchUserInfoDatabaseInDirectory(extractedID as String)
                                                self.imageChooseOpen()
                                            }
                                        }
                                    }
                                })
                            }
                            fbLoginManager.logOut()
                        }
                        
                    } else {
                        Alert(self, alertMessage:"Facebook login operation cancelled.")
                    }
                }
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

