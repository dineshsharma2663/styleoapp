//
//  SelectedImageCell.swift
//  StyleO
//
//  Created by Dinesh Kumar on 07/12/15.
//  Copyright © 2015 Dinesh Kumar. All rights reserved.
//

import UIKit

class SelectedImageCell: UICollectionViewCell {
    @IBOutlet weak var selectedItemImageView: UIImageView!
    
    @IBOutlet weak var deleteButton: UIButton!
}
