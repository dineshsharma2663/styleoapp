//
//  WearableTableCell.swift
//  StyleO
//
//  Created by Dinesh Kumar on 07/12/15.
//  Copyright © 2015 Dinesh Kumar. All rights reserved.
//

import UIKit

class WearableTableCell: UICollectionViewCell {

    @IBOutlet weak var tshirtImageView: UIImageView!
    @IBOutlet weak var pantImageView: UIImageView!
    @IBOutlet weak var dislikeButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

//    override func setSelected(selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }

}
